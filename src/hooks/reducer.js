const initStateList = [
  {
    text: 'Name 1',
    id: 0
  }
];

const ADD_NAME = 'ADD_NAME';
export const REMOVE_NAME = 'REMOVE_NAME';

export function listReducer(state = initStateList, action) {
  switch (action.type) {
    case ADD_NAME:
      return [
        ...state,
        {
          text: action.text,
          id: state.reduce((max, todo)=> Math.max(max,todo.id),-1) + 1
        }
      ]
    case REMOVE_NAME: 
      return state.filter(item => item.id !== action.id )
    default:
      return state
  }
} 

export const addName = text => ({ type: ADD_NAME, text })