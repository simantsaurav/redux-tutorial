import React from "react";
import { connect, useDispatch,useStore } from "react-redux";
import { bindActionCreators } from "redux";
import { addName, REMOVE_NAME } from "./reducer";

function MainComponent(props) {
  const {list, addName} = props;
  const dispatch = useDispatch();
  const store = useStore();

  console.log(store.getState());
  
  let input;

  const clickHandler = (e) => {
    let name = input.value;
    if(name !== ''){
      input.value = '';
      addName(name);
    }else{
      alert('please enter a name');
    }
  };

  return (
    <>
      <div className='container'>
        <div className='card'>
          <input ref={node =>{ input = node}} type='text' placeholder='Enter a Name..' className='header' />
          <button onClick={clickHandler}>Add</button>
          <div className='list'>
            <p>List of the Names added</p>
            <ul>
              {
                list.map(item => {
                  let id = item.id;
                  return (
                    <li 
                      key={id}
                      onClick={(e)=> { dispatch({type: REMOVE_NAME, id}) } }
                    >
                      {item.text}
                    </li>
                  )
                })
              }
              
            </ul>
          </div>
        </div>
      </div>
      <style jsx='true'>{`
        .container {
          padding: 10px 25px;
        }
        .card {
          border: 1px solid #aeaeae;
          padding: 10px;
          border-radius: 5px;
        }
        .header {
          height: 45px;
          font-size: 18px;
          width: 95%;
          margin-bottom: .5rem;
          padding: .25rem 1.5rem;
          font-weight: 300;
          line-height: 1.3;
          color: #495057;
          background-color: #fff;
          background-clip: padding-box;
          border: 1px solid #ced4da;
          border-radius: .25rem;
        }
        button {
          color: #fff;
          background-color: #50b167;
          border-color: #50b167;
          display: inline-block;
          font-weight: 400;
          text-align: center;
          vertical-align: middle;
          border: 1px solid transparent;
          padding: .375rem .75rem;
          font-size: 1rem;
          line-height: 1.3;
          border-radius: .25rem;
          cursor: pointer;
        }
      `}</style>
    </>
  );
  
}

const mapDispatchtoProps = dispatch => ({
  addName: bindActionCreators(addName, dispatch)
})

const mapStatetoProps = state => ({
  list: state
});

export default connect(mapStatetoProps, mapDispatchtoProps)(MainComponent);