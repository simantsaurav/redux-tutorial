import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import MainComponent from "./MainComponent";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { listReducer } from "./reducer";

const store = createStore(
  listReducer
  );

ReactDOM.render(
  <Provider store={store}>
    <MainComponent />
  </Provider>
  , document.getElementById('root')
);